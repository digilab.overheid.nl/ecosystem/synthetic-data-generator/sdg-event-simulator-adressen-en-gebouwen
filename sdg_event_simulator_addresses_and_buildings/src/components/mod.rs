use bevy::prelude::{Bundle, ResMut};
use bevy_turborand::{DelegatedRng, GlobalRng, RngComponent};
use chrono::NaiveDateTime;

mod address;
mod building;
mod date_time;
mod id;
mod surface;

pub use address::Address;
pub use address::HasBuilding;
pub use building::Building;
pub use building::HasAddress;
pub use date_time::DateTime;
pub use id::Id;
pub use surface::Surface;

use crate::data::{municipalities::MUNICIPALITIES, streets::STREET_NAMES};

#[derive(Bundle)]
pub struct BuildingBundle {
    pub building: Building,
    pub rng: RngComponent,
}

impl BuildingBundle {
    pub fn new(global_rng: &mut ResMut<GlobalRng>, time: NaiveDateTime) -> Self {
        let surface = Surface::new(global_rng);
        let constructed_at = DateTime::new(time);
        let mut rng = RngComponent::from(global_rng);

        let id = Id::new(&mut rng);

        let building = Building::new(id, surface, constructed_at);

        Self { building, rng }
    }
}

#[derive(Bundle)]
pub struct AddressBundle {
    pub address: Address,
    pub rng: RngComponent,
}

impl AddressBundle {
    pub fn new(global_rng: &mut ResMut<GlobalRng>) -> Self {
        let surface = Surface::new(global_rng);

        let mut rng = RngComponent::from(global_rng);

        let street = STREET_NAMES[rng.usize(0..STREET_NAMES.len())];
        let (municipality_code, municipality_name) =
            MUNICIPALITIES[rng.usize(0..MUNICIPALITIES.len())];
        let zip_code = format!(
            "{} {}{}",
            rng.usize(1000..10000),
            rng.uppercase(),
            rng.uppercase()
        );
        let house_number = rng.i32(1..=100);

        let use_house_number_addition = rng.chance(0.4);
        let mut house_number_addition: Option<String> = None;
        if use_house_number_addition {
            house_number_addition = Some(rng.uppercase().into());
        }

        let id = Id::new(&mut rng);

        let address = Address::new(
            id,
            street.into(),
            house_number,
            house_number_addition,
            zip_code,
            (municipality_code.into(), municipality_name.into()),
            String::from("woonfunctie"),
            surface.surface,
        );

        Self { address, rng }
    }
}
