use bevy::prelude::Component;

use super::Id;

#[derive(Component)]
pub struct HasBuilding(pub uuid::Uuid);

#[derive(Component, Clone)]
pub struct Address {
    pub id: Id,
    pub street: String,
    pub house_number: i32,
    pub house_number_addition: Option<String>,
    pub zip_code: String,
    pub municipality: (String, String),
    pub purpose: String,
    pub surface: i32,
}

impl Address {
    pub fn new(
        id: Id,
        street: String,
        house_number: i32,
        house_number_addition: Option<String>,
        zip_code: String,
        municipality: (String, String),
        purpose: String,
        surface: i32,
    ) -> Self {
        Self {
            id,
            street,
            house_number,
            house_number_addition,
            zip_code,
            municipality,
            purpose,
            surface
        }
    }
}
