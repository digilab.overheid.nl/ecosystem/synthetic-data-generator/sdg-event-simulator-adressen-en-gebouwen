use crate::{
    components::{Address, Building, HasAddress, HasBuilding},
    event::{BuildingAttached, BUILDING_ATTACHED_TOPIC},
    systems::init_building::get_seed_stream,
};
use bevy::prelude::{error, info, Commands, Entity, Query, Res};
use nats::jetstream::BatchOptions;
use sdg_event_simulator_common::{bevy::resources::nats_state::NatsState, event::Event};

pub fn load_seed_attached(
    mut commands: Commands,
    building_qry: Query<(&Building, Entity)>,
    address_qry: Query<(&Address, Entity)>,
    nats: Res<NatsState>,
) {
    info!("Load '{}' start", "seed attach registered");

    let stream = get_seed_stream(&nats.uri, BUILDING_ATTACHED_TOPIC);

    loop {
        let messages = stream.fetch(BatchOptions {
            batch: 100,
            expires: None,
            no_wait: true,
        });

        let messages = match messages {
            Ok(m) => m,
            Err(e) => {
                error!("Error loading events batch {}", e);
                break;
            }
        };

        let mut message_count = 0;

        for message in messages {
            let data = std::str::from_utf8(&message.data).unwrap();
            let event: Event = serde_json::from_str(data).unwrap();
            let event_data: BuildingAttached = serde_json::from_value(event.event_data).unwrap();

            let building_id = event.subject_ids[0];
            let address_id = event_data.address;

            if let Some((_, entity)) = building_qry
                .iter()
                .find(|(building, _)| building.id.uuid == building_id)
            {
                commands.entity(entity).insert(HasAddress(address_id));
            }

            if let Some((_, entity)) = address_qry
                .iter()
                .find(|(address, _)| address.id.uuid == building_id)
            {
                commands.entity(entity).insert(HasBuilding(building_id));
            }

            message.ack().unwrap();
            message_count += 1;
        }

        if message_count == 0 {
            info!("Load 'seed attached' end");
            break;
        }
    }
}

pub fn inspect_seeded(building_qry: Query<&Building>, address_qry: Query<&Address>) {
    info!("inspect seeded");

    info!("\t Total found:");
    info!("\t buildings: {}", building_qry.iter().count());
    info!("\t addresses: {}", address_qry.iter().count());
}
