mod address;
mod building;
mod init;
mod init_address;
mod init_building;

pub mod init_systems {
    pub use super::init_address::load_seed_addresses;
    pub use super::init_building::load_seed_buildings;

    pub use super::init::load_seed_attached;
    pub use super::init::inspect_seeded;
}

pub mod update_systems {
    pub use super::building::sim_attached;
    pub use super::building::sim_building_demolish;
    pub use super::building::sim_building_permit_changed;

    pub use super::address::sim_residential_object_formed;
    pub use super::address::sim_residential_object_withdraw;
}
