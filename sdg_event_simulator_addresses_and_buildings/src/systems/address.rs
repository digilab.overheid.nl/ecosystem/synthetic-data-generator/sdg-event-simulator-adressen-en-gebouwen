use crate::{
    components::{Address, AddressBundle, Building, HasAddress, HasBuilding},
    event::{
        address_registered_event, address_withdraw_event, ADDRESS_REGISTERED_TOPIC,
        ADDRESS_WITHDRAW_TOPIC,
    },
};
use bevy::prelude::{Commands, Entity, Query, Res, ResMut, With};
use bevy_turborand::{DelegatedRng, GlobalRng, RngComponent};
use chrono::Duration;
use sdg_event_simulator_common::{
    bevy::resources::{clock_state::ClockState, nats_state::NatsState},
    chance::PercentageChance,
};

pub fn sim_residential_object_formed(
    mut commands: Commands,
    mut global_rng: ResMut<GlobalRng>,
    clock_state: Res<ClockState>,
    nats: Res<NatsState>,
) {
    if do_building_permit(&mut global_rng, &clock_state) {
        let address = AddressBundle::new(&mut global_rng);

        let message = address_registered_event(
            &mut RngComponent::from(&mut global_rng),
            &clock_state.get_time(),
            &address.address,
        );

        commands.spawn(address);

        nats.connection
            .publish(ADDRESS_REGISTERED_TOPIC, message.to_string())
            .unwrap();
    }
}

pub fn do_building_permit(rng: &mut ResMut<GlobalRng>, clock_state: &ClockState) -> bool {
    let rand = rng.u64(0..u64::MAX);
    let chance = PercentageChance::new(daily_formed_chance())
        .scale(Duration::hours(24), clock_state.time_per_tick)
        .fraction_of_u64();

    rand < chance
}

fn daily_formed_chance() -> f64 {
    1.0
}

pub fn sim_residential_object_withdraw(
    mut commands: Commands,
    query: Query<(&Address, Entity, Option<&HasBuilding>)>,
    b_query: Query<(&Building, Entity), With<HasAddress>>,
    mut global_rng: ResMut<GlobalRng>,
    clock_state: Res<ClockState>,
    nats: Res<NatsState>,
) {
    let mut rng = RngComponent::from(&mut global_rng);
    for (address, entity, has_building) in query.iter() {
        if !do_withdraw(&mut global_rng, &clock_state) {
            continue;
        }

        let message = address_withdraw_event(&mut rng, &clock_state.get_time(), address);

        nats.connection
            .publish(ADDRESS_WITHDRAW_TOPIC, message.to_string())
            .unwrap();

        if let Some(has_building) = has_building {
            let res = b_query
                .iter()
                .find(|(building, _)| building.id.uuid == has_building.0);

            if let Some((_, entity)) = res {
                commands.entity(entity).remove::<HasAddress>();
            }
        }

        commands.entity(entity).remove::<Address>();
    }
}

fn do_withdraw(rng: &mut ResMut<GlobalRng>, clock_state: &ClockState) -> bool {
    let rand = rng.u64(0..u64::MAX);
    let chance = PercentageChance::new(daily_withdraw_chance())
        .scale(Duration::hours(24), clock_state.time_per_tick)
        .fraction_of_u64();

    rand < chance
}

fn daily_withdraw_chance() -> f64 {
    0.001
}
