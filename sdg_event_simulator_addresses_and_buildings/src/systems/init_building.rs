use crate::{
    components::{Building, DateTime, Id, Surface},
    event::{BuildingRegisteredData, BUILDING_REGISTERED_TOPIC},
};
use bevy::prelude::{error, info, Commands, Res};
use chrono::NaiveDateTime;
use nats::jetstream::{BatchOptions, ConsumerConfig, PullSubscribeOptions, PullSubscription};
use sdg_event_simulator_common::{
    bevy::resources::nats_state::NatsState,
    event::Event,
};

pub fn get_seed_stream(nats_uri: &str, topic: &str) -> PullSubscription {
    let tnc = nats::connect(nats_uri).expect("Could not connect to nats");

    let options = nats::JetStreamOptions::new();
    let stream = nats::jetstream::JetStream::new(tnc, options);

    stream
        .pull_subscribe_with_options(
            topic,
            &PullSubscribeOptions::new().consumer_config(ConsumerConfig {
                deliver_policy: nats::jetstream::DeliverPolicy::All,
                replay_policy: nats::jetstream::ReplayPolicy::Instant,
                filter_subject: topic.to_string(),
                ..Default::default()
            }),
        )
        .unwrap()
}

pub fn load_seed_buildings(mut commands: Commands, nats: Res<NatsState>) {
    info!("Load `seed building registered` start");

    let building_stream = get_seed_stream(&nats.uri, BUILDING_REGISTERED_TOPIC);

    loop {
        let messages = building_stream.fetch(BatchOptions {
            batch: 100,
            expires: None,
            no_wait: true,
        });

        let messages = match messages {
            Ok(m) => m,
            Err(e) => {
                error!("Error loading events batch {}", e);
                break;
            }
        };

        let mut message_count = 0;

        for message in messages {
            let data = std::str::from_utf8(&message.data).unwrap();
            let event: Event = serde_json::from_str(data).unwrap();
            let event_data: BuildingRegisteredData =
                serde_json::from_value(event.event_data).unwrap();

            let constructed_at = DateTime {
                date_time: NaiveDateTime::parse_from_str(
                    &event.occured_at,
                    "%Y-%m-%dT%H:%M:%S%.fZ",
                )
                .unwrap(),
            };

            let surface = Surface {
                surface: event_data.surface,
            };

            let id = Id {
                uuid: event.subject_ids[0],
            };

            commands.spawn(Building::new(id, surface, constructed_at));

            message.ack().unwrap();
            message_count += 1;
        }

        if message_count == 0 {
            info!("Load `seed data builings` end");
            break;
        }
    }
}
