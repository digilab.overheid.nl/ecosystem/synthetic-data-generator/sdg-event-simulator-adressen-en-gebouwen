use bevy::prelude::{Component, ResMut};
use bevy_turborand::{DelegatedRng, GlobalRng, RngComponent};

#[derive(Component, Clone, Copy)]
pub struct Surface {
    pub surface: i32,
}

impl Surface {
    pub fn new(global_rng: &mut ResMut<GlobalRng>) -> Self {
        let mut rng = RngComponent::from(global_rng);

        let surface = rng.i32(0..500);

        Self {
            surface,
        }
    }
}