use bevy::prelude::Component;
use chrono::{NaiveDateTime, format::{DelayedFormat, StrftimeItems}};

#[derive(Component, Clone, Copy)]
pub struct DateTime {
    pub date_time: NaiveDateTime,
}

impl DateTime {
    pub fn new(now: NaiveDateTime) -> Self {
        Self {
            date_time: now,
        }
    }

    pub fn format<'a>(&self, format: &'a str) -> DelayedFormat<StrftimeItems<'a>> {
        return self.date_time.format(format);
    }
}