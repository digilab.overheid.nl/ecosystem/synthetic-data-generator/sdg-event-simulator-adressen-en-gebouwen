use bevy_turborand::RngComponent;
use chrono::NaiveDateTime;
use sdg_event_simulator_common::{config::SdgConfig, event::Event};
use serde::{Deserialize, Serialize};
use serde_json::Value;

use crate::components::{Address, Building, Id};

pub const BUILDING_REGISTERED_TOPIC: &str = "events.frag.gebouw.Geregistreerd";
pub const BUILDING_ATTACHED_TOPIC: &str = "events.frag.gebouw.VerblijfsobjectToevoegen";
pub const BUILDING_DEMOLISHED_TOPIC: &str = "events.frag.gebouw.Gesloopt";

pub const ADDRESS_REGISTERED_TOPIC: &str = "events.frag.adres.Geregistreerd";
pub const ADDRESS_WITHDRAW_TOPIC: &str = "events.frag.adres.Intrekken";

#[derive(Serialize, Deserialize, Clone)]
pub struct BuildingRegisteredData {
    pub surface: i32,
}

#[derive(Serialize, Deserialize, Clone)]
pub struct AddressRegisteredData {
    #[serde(rename = "street")]
    pub street: String,
    #[serde(rename = "houseNumber")]
    pub house_number: i32,
    #[serde(rename = "houseNumberAddition")]
    pub house_number_addition: Option<String>,
    #[serde(rename = "zipCode")]
    pub zip_code: String,
    #[serde(rename = "municipality")]
    pub municipality: (String, String),
    #[serde(rename = "purpose")]
    pub purpose: String,
    #[serde(rename = "surface")]
    pub surface: i32,
}

#[derive(Serialize, Deserialize)]
pub struct BuildingAttached {
    pub address: uuid::Uuid,
}

pub fn building_registered_event(
    rng: &mut RngComponent,
    now: &NaiveDateTime,
    building: &Building,
) -> Value {
    let data = BuildingRegisteredData {
        surface: building.get_surface(),
    };

    let event = Event {
        id: Id::new(rng).uuid,
        occured_at: building
            .constructed_at
            .format(SdgConfig::TIMEFORMAT)
            .to_string(),
        registered_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        subject_ids: vec![building.id.uuid],
        event_type: "GebouwGeregistreerd".into(),
        event_data: serde_json::to_value(data).unwrap(),
    };

    serde_json::to_value(event).unwrap()
}

pub fn address_registered_event(
    rng: &mut RngComponent,
    now: &NaiveDateTime,
    address: &Address,
) -> Value {
    let data = AddressRegisteredData {
        street: address.street.clone(),
        house_number: address.house_number,
        house_number_addition: address.house_number_addition.clone(),
        zip_code: address.zip_code.clone(),
        municipality: address.municipality.clone(),
        purpose: address.purpose.clone(),
        surface: address.surface,
    };

    let event = Event {
        id: Id::new(rng).uuid,
        occured_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        registered_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        subject_ids: vec![address.id.uuid],
        event_type: "AdresGeregistreerd".into(),
        event_data: serde_json::to_value(data).unwrap(),
    };

    serde_json::to_value(event).unwrap()
}

pub fn building_attach_event(
    rng: &mut RngComponent,
    now: &NaiveDateTime,
    building: &Building,
    address_id: uuid::Uuid,
) -> Value {
    let data = BuildingAttached {
        address: address_id,
    };

    let event = Event {
        id: Id::new(rng).uuid,
        occured_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        registered_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        subject_ids: vec![building.id.uuid],
        event_type: "VerblijfsobjectToevoegenPand".into(),
        event_data: serde_json::to_value(data).unwrap(),
    };

    serde_json::to_value(event).unwrap()
}

pub fn building_demolish_event(
    rng: &mut RngComponent,
    now: &NaiveDateTime,
    building: &Building,
) -> Value {
    let event = Event {
        id: Id::new(rng).uuid,
        occured_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        registered_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        subject_ids: vec![building.id.uuid],
        event_type: "SloopAfgerond".into(),
        event_data: serde_json::to_value("").unwrap(),
    };

    serde_json::to_value(event).unwrap()
}

pub fn address_withdraw_event(
    rng: &mut RngComponent,
    now: &NaiveDateTime,
    address: &Address,
) -> Value {
    let event = Event {
        id: Id::new(rng).uuid,
        occured_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        registered_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        subject_ids: vec![address.id.uuid],
        event_type: "Intrekken".into(),
        event_data: serde_json::to_value("").unwrap(),
    };

    serde_json::to_value(event).unwrap()
}
