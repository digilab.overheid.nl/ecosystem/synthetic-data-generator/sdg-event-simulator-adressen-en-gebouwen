use crate::{
    components::{Address, Building, BuildingBundle, HasAddress, HasBuilding},
    event::{
        building_attach_event, building_demolish_event, building_registered_event,
        BUILDING_ATTACHED_TOPIC, BUILDING_DEMOLISHED_TOPIC, BUILDING_REGISTERED_TOPIC,
    },
};
use bevy::prelude::*;
use bevy_turborand::*;
use chrono::Duration;
use sdg_event_simulator_common::{
    bevy::resources::{clock_state::ClockState, nats_state::NatsState},
    chance::PercentageChance,
};

pub fn sim_building_permit_changed(
    mut commands: Commands,
    mut global_rng: ResMut<GlobalRng>,
    clock_state: Res<ClockState>,
    nats: Res<NatsState>,
) {
    if do_residential(&mut global_rng, &clock_state) {
        let now = clock_state.get_time();

        let building = BuildingBundle::new(&mut global_rng, now);

        let message = building_registered_event(
            &mut RngComponent::from(&mut global_rng),
            &now,
            &building.building,
        );

        commands.spawn(building);

        nats.connection
            .publish(BUILDING_REGISTERED_TOPIC, message.to_string())
            .unwrap();
    }
}

fn do_residential(rng: &mut ResMut<GlobalRng>, clock_time: &ClockState) -> bool {
    let rand = rng.u64(0..u64::MAX);
    let chance = PercentageChance::new(daily_formed_chance())
        .scale(Duration::hours(24), clock_time.time_per_tick)
        .fraction_of_u64();

    rand < chance
}

fn daily_formed_chance() -> f64 {
    1.0
}

pub fn sim_attached(
    mut commands: Commands,
    mut building_qry: Query<(&Building, Entity), Without<HasAddress>>,
    address_qry: Query<(&Address, Entity), Without<HasBuilding>>,
    mut global_rng: ResMut<GlobalRng>,
    clock_state: Res<ClockState>,
    nats: Res<NatsState>,
) {
    let mut rng = RngComponent::from(&mut global_rng);

    let mut address_iter = address_qry.iter();

    for (building, entity) in building_qry.iter_mut() {
        if do_attach(&mut global_rng, &clock_state) {
            // Try and find a new address
            if let Some((address, address_entity)) = address_iter.next() {
                let message = building_attach_event(
                    &mut rng,
                    &clock_state.get_time(),
                    building,
                    address.id.uuid,
                );

                nats.connection
                    .publish(BUILDING_ATTACHED_TOPIC, message.to_string())
                    .unwrap();

                commands.entity(entity).insert(HasAddress(address.id.uuid));
                commands
                    .entity(address_entity)
                    .insert(HasBuilding(building.id.uuid));
            }
        }
    }
}

fn do_attach(rng: &mut ResMut<GlobalRng>, clock_state: &ClockState) -> bool {
    let rand = rng.u64(0..u64::MAX);
    let chance = PercentageChance::new(daily_attach_chance())
        .scale(Duration::hours(24), clock_state.time_per_tick)
        .fraction_of_u64();

    rand < chance
}

fn daily_attach_chance() -> f64 {
    0.1
}

pub fn sim_building_demolish(
    mut commands: Commands,
    mut query: Query<(&Building, Entity, Option<&HasAddress>)>,
    a_query: Query<(&Address, Entity), With<HasBuilding>>,
    mut global_rng: ResMut<GlobalRng>,
    clock_state: Res<ClockState>,
    nats: Res<NatsState>,
) {
    let mut rng = RngComponent::from(&mut global_rng);

    for (building, entity, has_address) in query.iter_mut() {
        if !do_demolish(&mut global_rng, &clock_state) {
            continue;
        }

        let message = building_demolish_event(&mut rng, &clock_state.get_time(), building);

        nats.connection
            .publish(BUILDING_DEMOLISHED_TOPIC, message.to_string())
            .unwrap();

        // If we have an address decouple the HasBuilding component from the address entity
        if let Some(has_address) = has_address {
            let res = a_query
                .iter()
                .find(|(address, _)| address.id.uuid == has_address.0);

            if let Some((_, entity)) = res {
                commands.entity(entity).remove::<HasBuilding>();
            }
        }

        commands.entity(entity).remove::<Building>();
    }
}

fn do_demolish(rng: &mut ResMut<GlobalRng>, clock_state: &ClockState) -> bool {
    let rand = rng.u64(0..u64::MAX);
    let chance = PercentageChance::new(daily_destroy_chance())
        .scale(Duration::hours(24), clock_state.time_per_tick)
        .fraction_of_u64();

    rand < chance
}

fn daily_destroy_chance() -> f64 {
    0.001
}
