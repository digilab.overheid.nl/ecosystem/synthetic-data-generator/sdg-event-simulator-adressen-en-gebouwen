use bevy::prelude::{info, Commands, Res};
use nats::jetstream::BatchOptions;
use sdg_event_simulator_common::{bevy::resources::nats_state::NatsState, event::Event};
use tracing::error;

use crate::{
    components::{Address, Id},
    event::{AddressRegisteredData, ADDRESS_REGISTERED_TOPIC},
    systems::init_building::get_seed_stream,
};

pub fn load_seed_addresses(mut commands: Commands, nats: Res<NatsState>) {
    info!("Load '{}' start", "seed address registered");

    let address_stream = get_seed_stream(&nats.uri, ADDRESS_REGISTERED_TOPIC);

    loop {
        let messages = address_stream.fetch(BatchOptions {
            batch: 100,
            expires: None,
            no_wait: true,
        });

        let messages = match messages {
            Ok(m) => m,
            Err(e) => {
                error!("Error loading events batch {}", e);
                break;
            }
        };

        let mut message_count = 0;

        for message in messages {
            let data = std::str::from_utf8(&message.data).unwrap();
            let event: Event = serde_json::from_str(data).unwrap();
            let event_data: AddressRegisteredData =
                serde_json::from_value(event.event_data).unwrap();

            let id = Id {
                uuid: event.subject_ids[0],
            };

            let address = Address::new(
                id,
                event_data.street,
                event_data.house_number,
                event_data.house_number_addition,
                event_data.zip_code,
                event_data.municipality,
                event_data.purpose,
                event_data.surface,
            );

            commands.spawn(address);

            message.ack().unwrap();
            message_count += 1;
        }

        if message_count == 0 {
            info!("Load `seed data addresses` end");
            break;
        }
    }
}
