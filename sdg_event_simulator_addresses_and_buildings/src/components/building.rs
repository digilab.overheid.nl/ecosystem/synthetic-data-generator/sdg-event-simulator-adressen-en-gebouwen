use bevy::prelude::Component;

use super::{Id, Surface, DateTime};

#[derive(Component)]
pub struct HasAddress(pub uuid::Uuid);

#[derive(Component, Clone, Copy)]
pub struct Building {
    pub id: Id,
    pub surface: Surface,
    pub constructed_at: DateTime,
}

impl Building {
    pub fn new(id: Id, surface: Surface, constructed_at: DateTime) -> Self {
        Self {
            id,
            surface,
            constructed_at,
        }
    }

    pub fn get_surface(self) -> i32 {
        self.surface.surface
    }

}