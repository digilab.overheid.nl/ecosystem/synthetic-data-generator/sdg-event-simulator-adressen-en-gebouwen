use bevy::prelude::Component;
use bevy_turborand::{RngComponent, DelegatedRng};
use serde::{Serialize, Deserialize};
use ::uuid::{Builder as UuidBuilder, Uuid};

#[derive(Serialize, Deserialize, Component, Clone, Copy)]
pub struct Id {
    pub uuid: Uuid,
}
impl ToString for Id {
    fn to_string(&self) -> String {
        self.uuid.to_string()
    }
}
impl Id {
    pub fn new(rng: &mut RngComponent) -> Self {
        let mut bytes = [0; 16];
        rng.fill_bytes(&mut bytes);
        let stable_uuid = UuidBuilder::from_bytes(bytes).into_uuid();

        Self {
            uuid: stable_uuid,
        }
    }
}